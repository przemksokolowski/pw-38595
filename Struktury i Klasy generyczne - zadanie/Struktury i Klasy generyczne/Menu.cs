﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktury_i_Klasy_generyczne
{
    class Menu
    {
        public Menu()
        {
        }

        public void MenuShow()
        {

            Queue<Klient> Kolejka = new Queue<Klient>();
            Kolejka.Enqueue(new Klient(1,"Adam", "Andrzejewski"));
            Kolejka.Enqueue(new Klient(2,"Bartosz", "Baraniak"));
            Kolejka.Enqueue(new Klient(3,"Czesław", "Czajkowski"));
            Kolejka.Enqueue(new Klient(4,"Damian", "Dąbrowski"));
            Kolejka.Enqueue(new Klient(5,"Edward", "Ewasiński"));


            Dictionary<int, Ksiazka> Ksiegarnia = new Dictionary<int, Ksiazka>
            {
                { 1, new Ksiazka("Władca Pierścieni - Drużyna Pierścienia", "J.R.R. Tolkien", 59.99, 0000) },
                { 2, new Ksiazka("My, dzieci z dworca ZOO", "Christiane F.", 39.99, 1111) },
                { 3, new Ksiazka("Wesele", "Stanisław Wyspiański", 49.99, 2222) },
                { 4, new Ksiazka("Nemezis", "H.P. Lovecraft", 69.99, 3333) }
            };

            List<KeyValuePair<Ksiazka, Klient>> Sprzedaze = new List<KeyValuePair<Ksiazka, Klient>>();



            while (true)
            {
                Console.Clear();
                Console.WriteLine(">>> Menu główne <<<");
                Console.WriteLine("> 1 - Wyświetl książki ");
                Console.WriteLine("> 2 - Wyświetl kolejkę ");
                Console.WriteLine("> 3 - Sprzedaj książkę ");
                Console.WriteLine("> 4 - Wyświetl sprzedaże");
                Console.WriteLine("> 5 - Dodaj książkę");
                Console.WriteLine("> 6 - Usuń książkę");
                Console.WriteLine("> 7 - Dodaj klienta");
                Console.WriteLine("> 0 - koniec programu");
                ConsoleKeyInfo klawisz = Console.ReadKey();
                switch (klawisz.Key)
                {

                    case ConsoleKey.D1: //Wyświetla Książki
                        Console.Clear();
                        foreach (var item in Ksiegarnia)
                        {
                            Console.WriteLine("ID: "+ item.Key+ " ISBN: " + item.Value.ISBN + " Tytuł: " + item.Value.name + " Autor: " + item.Value.author + " Cena: " + item.Value.price);
                        }
                        Console.ReadKey();
                        break;

                    case ConsoleKey.D2: //Wyświetl Kolejkę
                        Console.Clear();
                        foreach (Klient item in Kolejka)
                        {
                            item.Wyswietl();
                        }
                        Console.ReadKey();
                        break;

                    case ConsoleKey.D3: //Sprzedaj Książkę
                        Console.Clear();
                        Console.WriteLine("Wybierz indeks książki: ");
                        int index = int.Parse(Console.ReadLine());
                        foreach (var item in Ksiegarnia)
                        {
                            if (index == item.Key)
                            {
                                KeyValuePair<Ksiazka, Klient> abc = new KeyValuePair<Ksiazka, Klient>(item.Value, Kolejka.Dequeue());
                                Sprzedaze.Add(abc);
                            }

                        }

                        Console.ReadKey();
                        break;

                    case ConsoleKey.D4: //Wyświetl sprzedaże
                        Console.Clear();
                        double piggybank = 0;
                        foreach (KeyValuePair<Ksiazka, Klient> item in Sprzedaze)
                        {
                            Console.WriteLine("Książka: " + item.Key.name + "\nKupił: " + item.Value.name + " " + item.Value.surname + "\n");
                            piggybank += item.Key.price;
                        }
                        Console.WriteLine("Łączny dochód to: " + piggybank);
                        Console.ReadKey();
                        break;
                    case ConsoleKey.D5: //Dodaj Książkę
                        Console.Clear();

                        int ISBN = 0;

                        for (; ; )
                        {
                            try
                            {
                                Console.WriteLine("Podaj numer ISBN: ");
                                ISBN = Int32.Parse(Console.ReadLine());
                            }
                            catch (System.FormatException)
                            {
                                continue;
                            }
                            break;
                        }

                       
                      

                        Console.WriteLine("Podaj tytuł: ");
                        string name = Console.ReadLine();
                        Console.WriteLine("Podaj autora: ");
                        string author = Console.ReadLine();
                        double price=0;

                        for (; ; )
                        {
                            try
                            {
                                Console.WriteLine("Wpisz cene: ");
                                price = double.Parse(Console.ReadLine());
                            }
                            catch (System.FormatException)
                            {
                                continue;
                            }
                            break;
                        }

                       int idx = Ksiegarnia.Last().Key + 1;
                        Ksiegarnia.Add(idx, new Ksiazka(name, author, price, ISBN));
                        Console.WriteLine("Dodano");
                        Console.ReadKey();
                        break;

                    case ConsoleKey.D6: //Usuń Książkę
                        Console.Clear();
                        Console.WriteLine("Podaj id ksiązki, którą chcesz usunąć: ");
                        int x = Int32.Parse(Console.ReadLine());  //konwersja z string do int
                        var ksiazka = Ksiegarnia.FirstOrDefault(a => a.Key == x);
                        Ksiegarnia.Remove(ksiazka.Key);
                        Console.ReadKey();
                        break;
                    case ConsoleKey.D7: //Dodaj Klienta
                        Console.Clear();
                        int id = Ksiegarnia.Last().Key + 1;
                        Console.WriteLine("Podaj imie");
                        string newName = Console.ReadLine();
                        Console.WriteLine("Podaj nazwisko");
                        string newSurname = Console.ReadLine();
                        Kolejka.Enqueue(new Klient(id,newName, newSurname));
                        Console.WriteLine("Dodano");
                        Console.ReadKey();
                        break;
                    case ConsoleKey.D0: // Koniec programu
                        Environment.Exit(0); break;
                    default: break;
                }
            }
        }
    }
}
