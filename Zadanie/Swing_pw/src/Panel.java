import javax.swing.*;
import java.awt.*;



public class Panel extends JFrame {

    public Panel() {

        JFrame frame = new JFrame("Kamil Piech, Natalia Jarzmicka 38563");
        frame.setContentPane(new Menu().panel);

        frame.setPreferredSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}