import javax.swing.*;
import java.awt.*;

public class Login extends JFrame {
    public int logged = Menu.logged;
    public Login() {

        JFrame frame = new JFrame("Logowanie");
        frame.setContentPane(new Login_form().panel);

        frame.setPreferredSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}