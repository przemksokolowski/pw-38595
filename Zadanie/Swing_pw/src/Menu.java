import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Menu extends JFrame {

    public static ArrayList<Klient> uczniowie = new ArrayList<Klient>();
    public static int logged = 0;
    public JButton zalogujButton;
    public JButton dodajUczniaButton;
    public JButton wyswietlUczniowButton;
    public JButton wylogujButton;
    public JPanel panel;
    private JButton wgrajDaneZPlikuButton;
    private JButton zapiszDaneDoPlikuButton;

    public Menu() {

        dodajUczniaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(logged == 1) {
                    new Dodaj();
                }else{
                    JOptionPane.showMessageDialog(null, "Zaloguj sie! Aby dodac ucznia!");
                }
            }
        });
        wyswietlUczniowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Lista();
            }
        });


        zalogujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(logged == 0) {
                    new Login();
                }else{
                    JOptionPane.showMessageDialog(null, "Jestes juz zalogowany!");
                }
            }
        });
        wylogujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Menu.logged == 1) {
                    Menu.logged = 0;
                    JOptionPane.showMessageDialog(null, "Wylogowano!");
                }else{
                    JOptionPane.showMessageDialog(null, "Nie jestes zalogowany, wiec nie mozesz sie wylogowac!");
                }
            }
        });


        wgrajDaneZPlikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(logged == 1) {
                try {
                    File inputFile = new File("data.xml");
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(inputFile);
                    doc.getDocumentElement().normalize();
                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    NodeList nList = doc.getElementsByTagName("person");
                    System.out.println("----------------------------");

                    for (int temp = 0; temp < nList.getLength(); temp++) {
                        Node nNode = nList.item(temp);

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode;
                            String imie = eElement.getElementsByTagName("imie").item(0).getTextContent();
                            String nazwisko = eElement.getElementsByTagName("nazwisko").item(0).getTextContent();
                            String numer = eElement.getElementsByTagName("numer").item(0).getTextContent();
                            int sprawdz = new Dodaj_form().sprawdz(numer);
                            if(sprawdz != 0) {
                                uczniowie.add(new Klient(imie, nazwisko, numer));
                            }
                        }
                    }
                } catch (Exception q) {
                    q.printStackTrace();
                }
                }else{
                    JOptionPane.showMessageDialog(null, "Zaloguj sie! Aby dodac dane z pliku!");
                }




            }
        });
        zapiszDaneDoPlikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(logged == 1) {
                    ArrayList<Klient> uczniowie = Menu.uczniowie;
                    try {
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder builder = factory.newDocumentBuilder();
                        Document doc = builder.newDocument();
                        doc.setXmlStandalone(true);
                        Element rootElement = doc.createElement("persons");
                        doc.appendChild(rootElement);
                        for (int i = 0; i < uczniowie.size(); i++) {
                            Element personElement = doc.createElement("person");
                            rootElement.appendChild(personElement);


                            Element nameElement = doc.createElement("imie");
                            nameElement.setTextContent("" + uczniowie.get(i).imie);
                            personElement.appendChild(nameElement);

                            Element nazwiskoElement = doc.createElement("nazwisko");
                            nazwiskoElement.setTextContent("" + uczniowie.get(i).nazwisko);
                            personElement.appendChild(nazwiskoElement);

                            Element numerElement = doc.createElement("numer");
                            numerElement.setTextContent("" + uczniowie.get(i).numer);
                            personElement.appendChild(numerElement);

                        }

                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transformer = transformerFactory.newTransformer();
                        DOMSource dom = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File("data.xml"));
                        transformer.transform(dom, result);
                        System.out.println("File employee was created.");
                    } catch (Exception z) {
                        System.out.println(z.getMessage());
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Zaloguj sie! Aby zapisac dane do pliku!");
                }
            }
        });
    }







    }

