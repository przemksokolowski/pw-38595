﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class KupnoBiletow : Form
    {
        public static List<Klient> Klienci = new List<Klient>();
        public static List<Filmy> Films = Dodaj.Films;
        public static List<Bilet> Bilety = new List<Bilet>();

        public static string selectedFilm;
        public static int allMoney;
        public static string radioName;
        private BindingList<Filmy> bindingList;

        public KupnoBiletow()
        {
            InitializeComponent();
        }

        private void getData()
        {
            this.bindingList = new BindingList<Filmy>(Films); //lista filmow
            comboBox1.DataSource = bindingList;
            comboBox1.DisplayMember = "title";
            comboBox1.ValueMember = "title";
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            int ticket = 0;

            if (radioButton1.Checked == true) //ulgowy
            {
                ticket = 20;
                radioName = radioButton1.Text;
            }
            else if(radioButton2.Checked == true) //normlany
            {
                ticket = 30;
                radioName = radioButton2.Text;
            }
            else
            {
                MessageBox.Show("Proszę wybrać rodzaj biletu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }

           
                    selectedFilm = comboBox1.GetItemText(comboBox1.SelectedItem);

                    Klienci.Add(new Klient(this.textBox1.Text, this.textBox2.Text, this.textBox3.Text, ticket));
                    Bilety.Add(new Bilet(selectedFilm, radioName));

                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    checkBox1.Checked = false;
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;

                    allMoney += ticket;
                    MessageBox.Show("Bilet został zakupiony");
                    this.Close();  
           
        }


        private void Form3_Load(object sender, EventArgs e)
        {
            getData();
        }
    }
}
