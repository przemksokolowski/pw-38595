﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            
        }

        Sprzedaz sprzedaz = new Sprzedaz();  
        ListaFilmow lista = new ListaFilmow();
        KupnoBiletow bilety = new KupnoBiletow();
        DodajUsun dodajusun = new DodajUsun();
        Zarobki zarobki = new Zarobki();

        private void button3_Click(object sender, EventArgs e) //zamknij
        {
            Environment.Exit(0);
        }

        private void button2_Click(object sender, EventArgs e) //dodaj/usun film
        {
            dodajusun.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e) //sprzedane bilety
        { 
            sprzedaz.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e) //lista filmow
        { 
            lista.ShowDialog();

        }

        private void button4_Click(object sender, EventArgs e) //kup bilety
        { 
            if (ListaFilmow.Films.Count != 0)
            {
                bilety.ShowDialog();
            }
            else
            {
                MessageBox.Show("Proszę dodać film. Aktualnie lista filmów jest pusta i nie ma możliwości kupna biletu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button6_Click(object sender, EventArgs e) //zarobki
        {
            zarobki.ShowDialog();
        }
    }
}

