﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class KupnoBiletow : Form
    {
        public static List<Klient> Klienci = new List<Klient>();
        public static List<Filmy> Films = Dodaj.Films;
        public static List<Bilet> Bilety = new List<Bilet>();

        public static string selectedFilm;
        public static int allMoney;
        public static string radioName;
        private BindingList<Filmy> bindingList;

        public KupnoBiletow()
        {
            InitializeComponent();
        }

        private void getData()
        {
            this.bindingList = new BindingList<Filmy>(Films); //lista filmow
            comboBox1.DataSource = bindingList;
            comboBox1.DisplayMember = "title";
            comboBox1.ValueMember = "title";
        }

        private bool CheckData() //sprawdzenie poprawnosci danych
        {
            Regex rx = new Regex( @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$"); //klasa zawierajaca wzorce ktore opisuja lancuchy symboli
            if (!textBox1.Text.All(c => char.IsDigit(c)) && !textBox2.Text.All(c => char.IsDigit(c)) && rx.IsMatch(textBox3.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int ticket = 0;

            if (radioButton1.Checked == true) //ulgowy
            {
                ticket = 20;
                radioName = radioButton1.Text;
            }
            else if(radioButton2.Checked == true) //normlany
            {
                ticket = 30;
                radioName = radioButton2.Text;
            }
            else
            {
                MessageBox.Show("Proszę wybrać rodzaj biletu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }

            if (this.textBox1.Text != "" && this.textBox2.Text != "" && this.textBox3.Text != "" )
            {

               if(CheckData()==true)
                {
                    selectedFilm = comboBox1.GetItemText(comboBox1.SelectedItem);

                    Klienci.Add(new Klient(this.textBox1.Text, this.textBox2.Text, this.textBox3.Text, ticket));
                    Bilety.Add(new Bilet(selectedFilm, radioName));

                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    checkBox1.Checked = false;
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;

                    allMoney += ticket;
                    MessageBox.Show("Bilet został zakupiony");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Wpisano błędne dane", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Proszę wprowadzić wszystkie dane","Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) 
        {
            doChecked();
        }

        private void doChecked() //akceptacja aby przejsc dalej 
        {
            button1.Enabled = checkBox1.Checked;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            getData();
            doChecked();
        }
    }
}
