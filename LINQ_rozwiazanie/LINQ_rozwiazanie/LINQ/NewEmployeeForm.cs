﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQ
{
    public partial class NewEmployeeForm : Form
    {
        BindingList<Employee> employeeList;

        public NewEmployeeForm(IBindingList list)
        {
            employeeList = (BindingList<Employee>) list;
            InitializeComponent();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void add_Click(object sender, EventArgs e)
        {
            if (employeeExists(firstName.Text, lastName.Text))
            {
                MessageBox.Show("This employee already exists.");
                return;
            }
            var checkedSex = sexPanel.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            Console.WriteLine(salaryLabel.Text);
            Employee employee = new Employee(firstName.Text, lastName.Text, checkedSex.Text, dateOfBirth.Value.Date, Int32.Parse(salary.Text));
            employeeList.Add(employee);
            this.Close();
        }

        private bool employeeExists(string first, string last)
        {
            return employeeList.Any(employee => employee.firstName == first && employee.lastName == last);
        }
    }
}
