﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LINQ
{
    public partial class SalariesForm : Form
    {
        BindingList<Employee> employeeList;
        BindingList<Payment> paymentList;

        public SalariesForm(IBindingList employeeList, IBindingList paymentList)
        {
            this.employeeList = (BindingList<Employee>)employeeList;
            this.paymentList = (BindingList<Payment>)paymentList;
            InitializeComponent();

            employeePicker.DataSource = employeeList;
            dataGrid.DataSource = paymentList;
        }

        private void addPayment_Click(object sender, EventArgs e)
        {
            Employee employee = (Employee)employeePicker.SelectedItem;
            Payment payment = new Payment(employee, dateTimePicker1.Value.Date, employee.salary);
            paymentList.Add(payment);
        }
    }
}
